var fs     = require('fs');
var path   = require('path');
var jsup   = require('jsup');
var semver = require('semver');

var newVersion = process.argv[2];
if (!newVersion) {
  throw new Error('Could not capture new version from `git-release` process');
}

var src = fs.readFileSync('package.json', 'utf8');
var s = jsup(src);
var oldVersion = s.get(['version']);

// Modify package JSON in place.
s.set(['version'], newVersion.replace(/^v/, ''));
fs.writeFileSync('package.json', s.stringify());

var poldVersion = semver.parse(oldVersion);
var pnewVersion = semver.parse(newVersion);

// Post to Twitter if major or minor version differ.
if (poldVersion.minor !== pnewVersion.minor ||
    poldVersion.major !== pnewVersion.major ||
    // Or if the version was not changed, meaning it's the first release.
    semver.eq(oldVersion, newVersion)) {
  if (fs.existsSync(path.join(__dirname, 'twitter.js'))) {
    require('./twitter');
  }
}
